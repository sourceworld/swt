import os,requests,ntpath,sys,mimetypes,youtube_dl
from urllib.parse import urlparse
from clint.textui import progress


try:

    url = input('URL: ')

    headers = {'User-Agent': ''}
    parse_url = urlparse(url)
    check_path = requests.head(str(url),headers=headers)
    
    if(check_path.status_code == requests.codes.ok or requests.codes.found):

        filename = urlparse(url).path
        filename = filename.replace('/','')
    
        ### Name for download file ###

        r = requests.get(url, allow_redirects=True)
        content_type = r.headers['content-type']
        print(content_type)
        if mimetypes.guess_extension(content_type):
            
            try:
                with open(filename, 'wb') as f:
                    total_length = int(r.headers.get('content-length'))
                    print('')
                    for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1): 
                        if chunk:
                            f.write(chunk)
                            f.flush()
                    print('')

                print('')

            except:

                try:

                    os.system('youtube-dl -U -f bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best -o %(title)s.%(ext)s '+str(url))

                except:

                    print('Download Failed')
        else:

            try:

                os.system('youtube-dl -U -f bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best -o %(title)s.%(ext)s '+str(url))

            except:

                 print('Download Failed')

except:
    exit