#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,re

ifile = input('Path to Emails file: ')

emails = []
emailsfile = open('ExportEmails.txt','a', encoding='utf8')

for line in open(ifile,'r', encoding='latin-1'):
	emails.append(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", line))


for email in emails:
	if email:
		email = str('\n'.join(email))
		emailsfile.write(email+'\n')
		print (email)	
	
emailsfile.close()
os.system('pause')